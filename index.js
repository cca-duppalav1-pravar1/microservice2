const express =require('express');
const app = express();
app.use(express.urlencoded({extended:false}))
const cors = require('cors')
app.use(cors())
var port = process.env.PORT || 8000;

app.use(express.urlencoded({extended:false}))
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port: ${port}`))
    
app.get('/', (req, res) => {
    res.send('Microservice GateWay by Ram Srikar Prava & Vamsi Duppala ');
})

app.get('/microservice2', (req, res) => {
    miles = req.query.miles;
    litres = req.query.litres;
    mpg = miles / (litres / 4.546);
    per100km = litres / ((miles * 1.60934) * 0.01);
    kmperlitre = miles * 1.60934 / litres;

    res.send({"mpg" : mpg , "per100km" : Number(per100km.toFixed(2)), "kmperlitre" : kmperlitre});
})